<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'middleware' => ['auth']
], function() {
    // Route::get('', [App\Http\Controllers\Backends\DashboardController::class, 'index'])->name('backends.dashboard');
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
	Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('room-types', App\Http\Controllers\Backends\RoomTypeController::class, [
        'as' => 'backends'
    ])->middleware('admin_role');

    Route::resource('rooms', App\Http\Controllers\Backends\RoomController::class, [
        'as' => 'backends'
    ]);

	Route::resource('posts', App\Http\Controllers\PostController::class);
	Route::delete('/posts/{post}/force-delete', [App\Http\Controllers\PostController::class, 'forceDestroy'])->name('posts.force_destroy');

	Route::resource('cats', App\Http\Controllers\CatController::class);
	Route::delete('/cats/{cat}/force-delete', [App\Http\Controllers\CatController::class, 'forceDestroy'])->name('cats.force_destroy');

	Route::resource('dboard', App\Http\Controllers\dboardController::class);
	Route::delete('/dboard/{dboard}/force-delete', [App\Http\Controllers\dboardController::class, 'forceDestroy'])->name('dboard.force_destroy');

	Route::resource('users', App\Http\Controllers\UserController::class);
	Route::delete('/users/{user}/force-delete', [App\Http\Controllers\UserController::class, 'forceDestroy'])->name('users.force_destroy');

    Route::get('/check-ins', [App\Http\Controllers\Backends\CheckInController::class, 'index'])->name('backends.check_ins.index');
});

/*
Route::get('/posts/{id}/show', [App\Http\Controllers\PostController::class, 'show'])->name('posts.show');;
Route::get('/posts', [App\Http\Controllers\PostController::class, 'index'])->name('posts.index');
Route::get('/posts/create', [App\Http\Controllers\PostController::class, 'create'])->name('posts.create');;
Route::post('/posts/store', [App\Http\Controllers\PostController::class, 'store'])->name('posts.store');;
Route::get('/posts/{id}/edit', [App\Http\Controllers\PostController::class, 'edit'])->name('posts.edit');;
Route::put('/posts/{post}/update', [App\Http\Controllers\PostController::class, 'update'])->name('posts.update');;
Route::delete('/posts/{post}/delete', [App\Http\Controllers\PostController::class, 'destroy'])->name('posts.destroy');
Route::delete('/posts/{post}/force-delete', [App\Http\Controllers\PostController::class, 'forceDestroy'])->name('posts.force_destroy');

Route::get('/categories/{id}/show', [App\Http\Controllers\CategoriesController::class, 'show'])->name('categories.show');;
Route::get('/categories', [App\Http\Controllers\CategoriesController::class, 'index'])->name('categories.index');
Route::get('/categories/create', [App\Http\Controllers\CategoriesController::class, 'create'])->name('categories.create');;
Route::post('/categories/store', [App\Http\Controllers\CategoriesController::class, 'store'])->name('categories.store');;
Route::get('/categories/{id}/edit', [App\Http\Controllers\CategoriesController::class, 'edit'])->name('categories.edit');;
Route::put('/categories/{category}/update', [App\Http\Controllers\CategoriesController::class, 'update'])->name('categories.update');;
Route::delete('/categories/{category}/delete', [App\Http\Controllers\CategoriesController::class, 'destroy'])->name('categories.destroy');
Route::delete('/categories/{category}/force-delete', [App\Http\Controllers\CategoriesController::class, 'forceDestroy'])->name('categories.force_destroy');

Route::get('/pages', [App\Http\Controllers\PageController::class, 'index'])->name('pages.index');
Route::get('/pages/create', [App\Http\Controllers\PageController::class, 'create'])->name('pages.create');
Route::post('/pages/store', [App\Http\Controllers\PageController::class, 'store'])->name('pages.store');
Route::get('/pages/{id}/edit', [App\Http\Controllers\PageController::class, 'edit'])->name('pages.edit');
Route::put('/pages/{page}/update', [App\Http\Controllers\PageController::class, 'update'])->name('pages.update');
Route::delete('/pages/{page}/delete', [App\Http\Controllers\PageController::class, 'destroy'])->name('pages.destroy');
Route::delete('/pages/{page}/force-delete', [App\Http\Controllers\PageController::class, 'forceDestroy'])->name('pages.force_destroy');
*/
// Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('users.index');
Route::get('/users/create', [App\Http\Controllers\UserController::class, 'create'])->name('users.create');
Route::post('/users/store', [App\Http\Controllers\UserController::class, 'store'])->name('users.store');
Route::get('/users/{id}/edit', [App\Http\Controllers\UserController::class, 'edit'])->name('users.edit');
Route::put('/users/{user}/update', [App\Http\Controllers\UserController::class, 'update'])->name('users.update');
Route::delete('/users/{user}/delete', [App\Http\Controllers\UserController::class, 'destroy'])->name('users.destroy');
Route::delete('/users/{user}/force-delete', [App\Http\Controllers\UserController::class, 'forceDestroy'])->name('users.force_destroy');


Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
});

Route::group(['middleware' => 'auth'], function () {
	// Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});
