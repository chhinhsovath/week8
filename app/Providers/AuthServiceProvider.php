<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Post;
use App\Policies\PostPolicy;
use App\Models\Categories;
use App\Policies\CategoryPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Post::class => PostPolicy::class,
        Categories::class => CategoryPolicy::class,
        
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // set user to access navigation
        Gate::define('crud-user', function(User $user) {
            return $user->role === ROLE_ADMIN;
        });

        // Gate::define('crud-post', function(User $user) {
        //     return $user->role === ROLE_ADMIN;
        // });
        
        // Gate::define('crud-category', function(User $user) {
        //     return $user->role === ROLE_ADMIN;
        // });

    }
}
