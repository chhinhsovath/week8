<?php

namespace App\Policies;

use App\Models\Cat;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CatPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cat  $cat
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function updateCat(User $user, Cat $cat)
    {
        return $user->role === ROLE_ADMIN || $user->id === $cat->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cat  $cat
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function deleteCat(User $user, Cat $cat)
    {
        return $user->role === ROLE_ADMIN;
    }
}
