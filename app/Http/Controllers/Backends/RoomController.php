<?php

namespace App\Http\Controllers\Backends;

use Illuminate\Support\Facades\Auth;
use App\Models\Room;
use App\Models\RoomType;
use App\Http\Requests\RoomStoreRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::get();
        return view('backends.rooms.index', [
            'rooms' => $rooms
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roomTypes = RoomType::pluck('title', 'id');
        return view('backends.rooms.create', [
            'roomTypes' => $roomTypes
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoomStoreRequest $request)
    {
        $roomData = $request->all();
        $roomData['user_id'] = Auth::id();
        Room::create($roomData);
        return redirect(route('backends.rooms.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Room $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        $this->authorize('updateRoom', $room);
        $roomTypes = RoomType::pluck('title', 'id');
        return view('backends.rooms.edit', [
            'roomTypes' => $roomTypes,
            'room' => $room
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Room $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        $this->authorize('updateRoom', $room);
        $room->update($request->all());
        return redirect(route('backends.rooms.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Room $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        $this->authorize('deleteRoom', $room);
        $room->delete();
        return redirect(route('backends.rooms.index'));
    }
}
