<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cat;
use Illuminate\Support\Facades\Auth;

class CatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $cats = Cat::all();
        return view('cats.index', [
            'cats' => $cats
        ]);
    }

    public function create()
    {
      return view('cats.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);
        $cat = new Cat();
        $cat->title = $request->title;
        $cat->user_id = Auth::id();

        $cat->save();
        return redirect(route('cats.index'))->with('success','Category created successfully!');
    }

    public function show($id)
    {
        $cat = Cat::find($id);
        if (empty($cat)) {
            return redirect(route('cats.index'));
        }
        return view('cats.show')->with('cat', $cat);
    }

    public function edit(Cat $cat)
    {
           $this->authorize('updateCat', $cat);
            return view('cats.edit', [
                'cat' => $cat,
            ]);
    }

    public function update(Cat $cat, Request $request)
    {
        $this->authorize('updateCat', $cat);
        $request->validate([
            'title' => 'required',
        ]);
        $cat->title = $request->title;

        $cat->save();
        return redirect(route('cats.index'))->with('success','Category updated successfully!');
    }

    public function destroy(Cat $cat)
    {
        $this->authorize('deleteCat', $cat);
        $cat->delete();
        return redirect(route('cats.index'))->with('success','Category deleted successfully!');
    }

    public function forceDestroy(Cat $cat)
    {
        $this->authorize('deleteCat', $cat);
        $cat->forceDelete();
        return redirect(route('cats.index'))->with('success','Category deleted successfully!');
    }
}
