<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Cat;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $posts = Post::all();
        return view('posts.index', [
            'posts' => $posts
        ]);
    }

    public function create()
    {
      $cats = Cat::pluck('title', 'id');
      return view('posts.create', [
          'cats' => $cats
      ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
        $post = new Post();
        $post->title = $request->title;
        $post->body = $request->body;
        $post->published_at = $request->published_at;
        $post->user_id = Auth::id();
        $post->category_id = $request->category_id;

        $post->save();
        return redirect(route('posts.index'))->with('success','Post created successfully!');
    }

    public function show($id)
    {
        $post = Post::find($id);
        if (empty($post)) {
            return redirect(route('posts.index'));
        }
        return view('posts.show')->with('post', $post);
    }

    public function edit(Post $post)
    {
           $this->authorize('updatePost', $post);
           $cats = Cat::pluck('title', 'id');
            return view('posts.edit', [
                'post' => $post,
                'cats' => $cats
            ]);
    }

    public function update(Post $post, Request $request)
    {
        $this->authorize('updatePost', $post);
        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->published_at = $request->published_at;
        $post->category_id = $request->category_id;

        $post->save();
        return redirect(route('posts.index'))->with('success','Post updated successfully!');
    }

    public function destroy(Post $post)
    {
        $this->authorize('deletePost', $post);
        $post->delete();
        return redirect(route('posts.index'))->with('success','Post deleted successfully!');
    }

    public function forceDestroy(Post $post)
    {
        $this->authorize('deletePost', $post);
        $post->forceDelete();
        return redirect(route('posts.index'))->with('success','Post deleted successfully!');
    }
}
