<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\Dboard;
use App\Models\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        //Call Backend Procedure
        // DB::select("CALL update_dashboard()");
        DB::select("CALL update_dboard()");

        $posts = Post::all();
        $dboards = Dboard::all();

        // return view('dashboard')->with('');
         return view('dashboard', [
                'posts' => $posts,
                'dboards' => $dboards
            ]);
    }
}
