<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'title',
        'body',
        'published_at',
        'category_id',
    ];

    public function category()
    {
        return $this->belongsTo(Cat::class,'category_id','id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class,'user_id','id');
        //return $this->belongsTo(User::class, 'foreign_key', 'owner_key');
    }
}
