<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cat extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'title',
    ];

    public function creator()
    {
        return $this->belongsTo(User::class,'user_id','id');
        //return $this->belongsTo(User::class, 'foreign_key', 'owner_key');
    }
}
