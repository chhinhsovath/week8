<?php
namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'title' => 'Is your Router Outdated and Causing you More Problems?',
            'body' => 'Outdated routers and interference may be causing issues with your Wi-Fi connectivity and slowing down your internet speeds. A slow Internet connection can be very frustrating. There are many factors that affect your internet speed. Sometimes you just don’t subscribe to the speed you need for the number of devices being used, and sometimes your computer or device has an issue. ',
            'published_at' => now(),
            'category_id' => 2,
            'user_id' => 2,
            'created_at' => now(),
        ],
      );

      DB::table('posts')->insert([
          'title' => 'Bat viruses being studied in Cambodia to gain new coronavirus pandemic insights',
          'body' => 'Bat researchers in Cambodia are on a mission to help track the origins of the pandemic by collecting samples from horseshoe bats in the country’s northern regions near Laos.',
          'published_at' => now(),
          'category_id' => 2,
          'user_id' => 2,
          'created_at' => now(),
      ],
    );

    DB::table('posts')->insert([
        'title' => 'Cambodian children face crisis from poor diets',
        'body' => 'Cambodian children under the age of two are not getting the food or nutrients they need to thrive and grow well, leading to irreversible developmental harm, according to a new report released by UNICEF’s global office today.',
        'published_at' => now(),
        'category_id' => 3,
        'user_id' => 3,
        'created_at' => now(),
    ],
  );
  DB::table('posts')->insert([
      'title' => 'Basic CRUD Blog Tutorial with Bootstrap',
      'body' => 'In the previous tutorial, we learned how to setup authentication in Laravel 8 using Bootstrap. In this article, we will build on top of that. We will create basic CRUD web application — CRUD means Create, Read, Update, & Delete. Thus, we will learn how to do these operations in Laravel 8. Note: I am not explaining how to install and start project here since this article is sequel to previous post. If you are new here, please start from Laravel 8 Authentication using Bootstrap 4.',
      'published_at' => now(),
      'category_id' => 3,
      'user_id' => 3,
      'created_at' => now(),
  ],
);

    }
}
