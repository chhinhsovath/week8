<?php
namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('cats')->insert([
        //     'title' => 'Local News',
        //     'user_id' => 1,
        //     'created_at' => now(),
        //     ],
        // );
        // DB::table('cats')->insert([
        //     'title' => 'International News',
        //     'user_id' => 1,
        //     'created_at' => now(),
        //     ],
        // );
        // DB::table('cats')->insert([
        //     'title' => 'Technology',
        //     'user_id' => 2,
        //     'created_at' => now(),
        //     ],
        // );
        // DB::table('cats')->insert([
        //     'title' => 'Agriculture',
        //     'user_id' => 2,
        //     'created_at' => now(),
        //     ],
        // );
        // DB::table('cats')->insert([
        //     'title' => 'Accounting',
        //     'user_id' => 3,
        //     'created_at' => now(),
        //     ],
        // );
        // DB::table('cats')->insert([
        //             'title' => 'Marketing',
        //             'user_id' => 3,
        //             'created_at' => now(),
        //     ],
        // );
        DB::table('cats')->insert([
                    'title' => 'Cat Editor A',
                    'user_id' => 2,
                    'created_at' => now(),
            ],
        );
        DB::table('cats')->insert([
                    'title' => 'Cat Editor Aa',
                    'user_id' => 2,
                    'created_at' => now(),
            ],
        );
        DB::table('cats')->insert([
                    'title' => 'Cat Editor B',
                    'user_id' => 3,
                    'created_at' => now(),
            ],
        );
        DB::table('cats')->insert([
                    'title' => 'Cat Editor Bb',
                    'user_id' => 3,
                    'created_at' => now(),
            ],
        );
    }
}
