<?php
namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DasboardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dashboard')->insert([
            'users' => 0,
            'posts' => 0,
            'cats' => 0,
            'created_at' => now(),
            ],
        );
        
    }

        /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "DROP PROCEDURE IF EXISTS `update_dashboard`;
            CREATE PROCEDURE `update_dashboard` (IN idx int)
            BEGIN
                UPDATE dashboard as db SET db.users=(SELECT COUNT(*) FROM users WHERE users.deleted_at IS NULL) WHERE db.id=1;
                UPDATE dashboard as db SET db.posts=(SELECT COUNT(*) FROM posts WHERE posts.deleted_at IS NULL) WHERE db.id=1;
                UPDATE dashboard as db SET db.cats=(SELECT COUNT(*) FROM cats WHERE cats.deleted_at IS NULL) WHERE db.id=1;
            END;";
  
        \DB::unprepared($procedure);
    }
  
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         
    }
}
