<?php

return [
    'init_users' => [
        [
            'role' => ROLE_ADMIN,
            'name' => env('USER_ADMIN_NAME'),
            'email' => env('USER_ADMIN_EMAIL'),
            'password' => env('USER_ADMIN_PASSWORD'),
        ],
        [
            'role' => ROLE_STAFF,
            'name' => env('USER_STAFF_NAME'),
            'email' => env('USER_STAFF_EMAIL'),
            'password' => env('USER_STAFF_PASSWORD'),
        ],
        [
            'role' => ROLE_EDITOR,
            'name' => env('USER_EDITOR_NAME'),
            'email' => env('USER_EDITOR_EMAIL'),
            'password' => env('USER_EDITOR_PASSWORD'),
        ]
    ],
    'pagination' => 25
];