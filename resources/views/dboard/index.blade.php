@extends('layouts.app', ['activePage' => 'post', 'titlePage' => __('Post')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Post</h4>
                            <p class="card-category"> Here you can manage post</p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 text-right">
                                    <a href="{{route("posts.create")}}" class="btn btn-sm btn-primary">Add post</a>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                        <tr>
                            {{-- <th scope="col">#</th> --}}
                            <th>Id</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Body</th>
                            <th>Creator</th>
                            <th>Published At</th>
                            <!-- <th>Created at</th> -->
                            <th colspan="2" class="text-right">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $post)
                            <tr>
                                {{-- <td scope="row">{{ $loop->index + 1 }}</td> --}}
                                <td>{{ $post->id }}</td>
                                <td>{{ $post->title }}</td>
                                <td>{{ $post->category->title }}</td>
                                <td>{{ $post->creator->name }}</td>
                                <td>{{ $post->body }}</td>
                                <td>{{ date('Y-m-d', strtotime($post->published_at)) }}</td>
                                <!-- <td>{{ date('Y-m-d', strtotime($post->created_at)) }}</td> -->
                                <td width="250px;" class="td-actions text-right">
                                    <a href="{{ route('posts.show', $post->id) }}" class="btn btn-default">Show</a>
                                    
                                    @can('updatePost', $post)
                                        <a href="{{ route('posts.edit', $post->id) }}"  class="btn btn-primary">Edit</a>
                                    @endcan
                                    @can('deletePost', $post)
                                        <a href="#" class="btn-delete-page btn btn-warning" data-form-id="frmPageDelete{{ $post->id }}">
                                            <form id="frmPageDelete{{ $post->id }}" action="{{ route('posts.destroy', $post->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            Delete
                                        </a>
                                    
                                        <a href="#" class="btn-delete-page btn btn-danger" data-form-id="frmPageForceDelete{{ $post->id }}">
                                            <form id="frmPageForceDelete{{ $post->id }}" action="{{ route('posts.force_destroy', $post->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            Force Delete
                                        </a>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            // Apply onClick event to delete page
            $('.btn-delete-page').click(function() {
                if (confirm('Are you sure?')) {
                    let formId = $(this).data('form-id');
                    // Submit form
                    $('#'+ formId).submit()
                }
            });
        });
    </script>
@endpush
