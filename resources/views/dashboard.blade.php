@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])
{{-- Helper with collection --}}
{{-- https://stackoverflow.com/questions/41366092/property-title-does-not-exist-on-this-collection-instance  --}}
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
              <div class="card-icon">
                <i class="material-icons">post_add</i>
              </div>
              <p class="card-category">Post</p>
              <h3 class="card-title"> {{ $dboards[0]->posts }}</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons text-danger">warning</i>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i class="material-icons">category</i>
              </div>
              <p class="card-category">Category</p>
              <h3 class="card-title">{{ $dboards[0]->cats }}</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons">date_range</i>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
              <div class="card-icon">
                <i class="material-icons">perm_identity</i>
              </div>
              <p class="card-category">User</p>
              <h3 class="card-title">{{ $dboards[0]->users }}</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons">local_offer</i>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
                <i class="fa fa-twitter"></i>
              </div>
              <p class="card-category">Page Visit</p>
              <h3 class="card-title">+245</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons">update</i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">

        @foreach ($posts as $post)
            
          <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                {{ $post->title }}
              </div>
            </div>
            <div class="card-body">
              <h3 class="card-title"> {{ $post->category->title }}</h3>
              <p class="card-category">{{ $post->body }}</p>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons text-danger">event_note</i>{{ $post->published_at }}
              </div>
            </div>
          </div>
        </div>

        @endforeach

      </div>
    </div>
  </div>      
@endsection

@push('js')
  <script>
    $(document).ready(function() {
    });
  </script>
@endpush