@extends('layouts.app', ['activePage' => 'post', 'titlePage' => __('Post')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Post</h4>
                            <p class="card-category"> Here you can create new post</p>
                        </div>

                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12 text-right">
                                                <a href="{{route('posts.index')}}" class="btn btn-sm btn-primary">Back</a>
                                            </div>
                                        </div>

                                        @if (session('status'))
                                            <div class="alert alert-success" role="alert">
                                                {{ session('status') }}
                                            </div>
                                        @endif

                                        <form action="{{route('posts.store')}}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <label for="">Post Title</label>
                                                <input type="text" name="title" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label for="">Post Body</label>
                                                <textarea name="body" id="" cols="30" rows="10" class="form-control"></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label for="">Publish At</label>
                                                <input type="date" name="published_at" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <select class="form-control" name="category_id" required>
                                                    <option value="" selected>--- Category ---</option>
                                                    @foreach($cats as $id => $title)
                                                        <option value="{{ $id }}">{{ $title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

    </div>
@endsection
