@extends('layouts.app', ['activePage' => 'post', 'titlePage' => __('Post')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Post</h4>
                            <p class="card-category"> Here you can view post</p>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 text-right">
                                    <a href="{{route('posts.index')}}" class="btn btn-sm btn-primary">Back</a>
                                </div>
                            </div>
                                        @if (session('status'))
                                            <div class="alert alert-success" role="alert">
                                                {{ session('status') }}
                                            </div>
                                        @endif

                                        <h2>{{$post->title}}</h2>

                                        <p>Published At: {{date('Y-m-d', strtotime($post->published_at))}}</p>
                                        <br>
                                        <div>
                                            {{$post->body}}
                                        </div>
                                    </div>
                                </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
