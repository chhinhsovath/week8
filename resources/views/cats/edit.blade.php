@extends('layouts.app', ['activePage' => 'category', 'titlePage' => __('Category')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Category</h4>
                            <p class="card-category"> Here you can update Cagetory</p>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 text-right">
                                    <a href="{{route('cats.index')}}" class="btn btn-sm btn-primary">Back</a>
                                </div>
                            </div>
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                                <form action="{{ route('cats.update',$cat->id) }}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label for="">Category Title</label>
                                        <input type="text" name="title" class="form-control" value="{{$cat->title}}">
                                    </div>

                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection
