@extends('layouts.app', ['activePage' => 'post', 'titlePage' => __('Post')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Category</h4>
                            <p class="card-category"> Here you can view category</p>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 text-right">
                                    <a href="{{route('cats.index')}}" class="btn btn-sm btn-primary">Back</a>
                                </div>
                            </div>
                                        @if (session('status'))
                                            <div class="alert alert-success" role="alert">
                                                {{ session('status') }}
                                            </div>
                                        @endif

                                        <h2>{{$cat->title}}</h2>
                                    </div>
                                </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
