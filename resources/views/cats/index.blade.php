@extends('layouts.app', ['activePage' => 'category', 'titlePage' => __('Category')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Category</h4>
                            <p class="card-category"> Here you can manage Category</p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 text-right">
                                    <a href="{{route("cats.create")}}" class="btn btn-sm btn-primary">Add Category</a>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                        <tr>
                            {{-- <th scope="col">#</th> --}}
                            <th>Id</th>
                            <th>Title</th>
                            <th>Creator</th>
                            <th colspan="2" class="text-right">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cats as $cat)
                            <tr>
                                {{-- <td scope="row">{{ $loop->index + 1 }}</td> --}}
                                <td>{{ $cat->id }}</td>
                                <td>{{ $cat->title }}</td>
                                <td>{{ $cat->creator->name }}</td>
                                <td width="250px;" class="td-actions text-right">
                                    <a href="{{ route('cats.show', $cat->id) }}" class="btn btn-default">Show</a>
                                    
                                    @can('updateCat', $cat)
                                        <a href="{{ route('cats.edit', $cat->id) }}"  class="btn btn-primary">Edit</a>
                                    @endcan
                                    @can('deleteCat', $cat)
                                        <a href="#" class="btn-delete-page btn btn-warning" data-form-id="frmPageDelete{{ $cat->id }}">
                                            <form id="frmPageDelete{{ $cat->id }}" action="{{ route('cats.destroy', $cat->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            Delete
                                        </a>
                                    
                                        <a href="#" class="btn-delete-page btn btn-danger" data-form-id="frmPageForceDelete{{ $cat->id }}">
                                            <form id="frmPageForceDelete{{ $cat->id }}" action="{{ route('cats.force_destroy', $cat->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            Force Delete
                                        </a>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            // Apply onClick event to delete page
            $('.btn-delete-page').click(function() {
                if (confirm('Are you sure?')) {
                    let formId = $(this).data('form-id');
                    // Submit form
                    $('#'+ formId).submit()
                }
            });
        });
    </script>
@endpush
