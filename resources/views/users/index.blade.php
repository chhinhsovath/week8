@extends('layouts.app', ['activePage' => 'user-management', 'titlePage' => __('User Profile')])

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Users</h4>
                            <p class="card-category"> Here you can manage users</p>
                        </div>
                        <div class="card-body">
                            <div class="row d-none">
                                <div class="col-12 text-right">
                                    <a href="#" class="btn btn-sm btn-primary">Add user</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                    <tr>
                                        <th>
                                            ID
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th class="text-right d-none">
                                            Actions
                                        </th>
                                    </tr></thead>
                                    <tbody>
                                    @foreach($users as $user)
                                    <tr>
                                        <td>
                                            {{ $user->id }}
                                        </td>
                                        <td>
                                            {{ $user->name }}
                                        </td>
                                        <td>
                                            {{ $user->email }}
                                        </td>
                                        <td width="250px;" class="td-actions text-right d-none">
                                            <a href="{{ route('users.show', $user->id) }}" class="btn btn-default">Show</a>
                                            <a href="{{ route('users.edit', $user->id) }}"  class="btn btn-primary">Edit</a>
                                            <a href="#" class="btn-delete-page btn btn-warning" data-form-id="frmPageDelete{{ $user->id }}">
                                                <form id="frmPageDelete{{ $user->id }}" action="{{ route('users.destroy', $user->id) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                                Delete
                                            </a>
                                            <a href="#" class="btn-delete-page btn btn-danger" data-form-id="frmPageForceDelete{{ $user->id }}">
                                                <form id="frmPageForceDelete{{ $user->id }}" action="{{ route('users.force_destroy', $user->id) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                                Force Delete
                                            </a>
                                        </td>
                                        {{-- <td class="td-actions text-right">
                                            <a rel="tooltip" class="btn btn-success btn-link" href="#" data-original-title="" title="">
                                                <i class="material-icons">edit</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                        </td> --}}
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
