<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="#" class="simple-text logo-normal">
      {{ __('Week8 Assignment') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
       <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
            <a class="nav-link" href="{{ route('profile.edit') }}">
                {{--<span class="sidebar-mini"> UP </span>--}}
                <i class="material-icons">person</i>
                <span class="sidebar-normal">{{ __('User profile') }} </span>
            </a>
        </li>
        {{-- SET User Access Navigation --}}
        @can('crud-user')
        <li class="nav-item{{ $activePage == 'user-management' ? ' active' : '' }}">
            <a class="nav-link" href="{{ route('users.index') }}">
                {{--<span class="sidebar-mini"> UM </span>--}}
                <i class="material-icons">groups</i>
                <span class="sidebar-normal"> {{ __('User Management') }} </span>
            </a>
        </li>
        @endcan
      {{-- <li class="nav-item{{ $activePage == 'table' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('table') }}">
          <i class="material-icons">content_paste</i>
            <p>{{ __('Table List') }}</p>
        </a>
      </li> --}}
      <li class="nav-item{{ $activePage == 'post' ? ' active' : '' }}">
          <a class="nav-link" href="{{ route('posts.index') }}">
              <i class="material-icons">article</i>
              <p>{{ __('Post') }}</p>
          </a>
      </li>
      <li class="nav-item{{ $activePage == 'category' ? ' active' : '' }}">
          <a class="nav-link" href="{{ route('cats.index') }}">
              <i class="material-icons">category</i>
              <p>{{ __('Category') }}</p>
          </a>
      </li>

    </ul>
  </div>
</div>
